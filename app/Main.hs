{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Control.Applicative                  ((<|>))
import           Control.Monad                        (forever)
import           Control.Monad.IO.Class               (liftIO)
import           Data.Aeson                           (FromJSON, eitherDecode)
import           Data.ByteString.Lazy.Char8           (pack)
import           Data.Monoid
import           Data.Text                            (Text)
import qualified Data.Text.IO                         as T
-- import Network.Monitoring.Riemann.Json ()
import           Network.Monitoring.Riemann.Event     (Event)
import           Network.Monitoring.Riemann.TCPClient (tcpClient)
import           Network.Socket                       (withSocketsDo)
import qualified Network.URI.Encode                   as URI
import qualified Network.WebSockets                   as WS
import           Options.Applicative                  (Parser, argument, auto,
                                                       command, eitherReader,
                                                       execParser, fullDesc,
                                                       header, help, helper,
                                                       hsubparser, info, long,
                                                       metavar, option,
                                                       progDesc, short, str,
                                                       strOption, value, (<**>))

import           Rcl

data Command =
    Send SendSpec
  | SendJSON SendJSONSpec
  | SubScribe SubscribeSpec

data SubscribeSpec = SubscribeSpec
  { subHost  :: String
  , subPort  :: Int
  , subQuery :: String
  }


data SendSpec = SendSpec
   { sendHost    :: String
   , sendPort    :: Int
   , sendService :: EventName
   , sendQuery   :: Attrs
   }

data SendJSONSpec = SendJSONSpec
  { sendJSONHost  :: String
  , sendJSONPort  :: Int
  , sendJSONEvent :: Event
  }

hostP = strOption
          ( long "host"
         <> short 'h'
         <> metavar "HOST"
         <> help "Riemann Server Host"
         <> value "127.0.0.1"
          )

portP n = option auto
          ( long "port"
         <> short 'p'
         <> metavar "PORT"
         <> help "Riemann Server Port"
         <> value n
          )

jsonP :: FromJSON a => String -> String -> Parser a
jsonP meta hlp = argument (eitherReader (eitherDecode . pack))
                  ( metavar meta
                 <> help hlp
                  )

subscribeParser :: Parser SubscribeSpec
subscribeParser = SubscribeSpec
  <$> hostP
  <*> portP 5556
  <*> argument str ( metavar "QUERY" <> help "Riemann Subscription Query" <> value "true" )


sendParser :: Parser SendSpec
sendParser = SendSpec
  <$> hostP
  <*> portP 5555
  <*> (EventName <$> strOption
                      ( metavar "SERVICE NAME"
                    <> long "service"
                    <> short 's'
                    <> help "Name of the service"
                      )
      )
  <*> jsonP
        "ATTRIBUTE KEY VALUES"
        "A JSON map of keys and values that will be encoded into event attributes"

sendJSONParser :: Parser SendJSONSpec
sendJSONParser = SendJSONSpec
  <$> hostP
  <*> portP 5555
  <*> jsonP
        "JSON reimann event"
        "JSON object that can be decoded to a riemann event"

commandParser :: Parser Command
commandParser = hsubparser $
     (command "send" (info (Send <$> sendParser) (progDesc "Send an event to riemann")))
  <> (command "send-json" (info (SendJSON <$> sendJSONParser) (progDesc "Send an event to riemann")))
  <> (command "subs" (info (SubScribe <$> subscribeParser) (progDesc "Subscribe to riemann events")))

app :: WS.ClientApp ()
app conn = forever $ do
  msg <- WS.receiveData conn
  liftIO . T.putStrLn $ msg

mkPath :: String -> String
mkPath q = "/index?subscribe=true&query=" <> (URI.encode q)

runSubscribe :: SubscribeSpec -> IO ()
runSubscribe (SubscribeSpec host port query) = withSocketsDo $ WS.runClient host port (mkPath query) app

runSendJSON :: SendJSONSpec -> IO ()
runSendJSON (SendJSONSpec host port event) = do
  c <- tcpClient host port
  sendEventFull c event

runSend :: SendSpec -> IO ()
runSend (SendSpec host port service attrs) = do
  c <- tcpClient host port
  sendEventAttrs c service attrs

runCommand :: Command -> IO ()
runCommand (Send s)      = runSend s
runCommand (SendJSON s)      = runSendJSON s
runCommand (SubScribe s) = runSubscribe s


main :: IO ()
main = runCommand =<< execParser opts
  where
    opts = info (commandParser <**> helper)
      ( fullDesc
      <> header "rcl - Riemann Command Line Client"
      )
