{ mkDerivation, base, binary, bytestring, containers, criterion
, hostname, kazura-queue, network, protocol-buffers
, protocol-buffers-descriptor, stdenv, text, time, unagi-chan
}:
with import <nixpkgs> {};

mkDerivation {
  pname = "hriemann";
  version = "0.2.1.0";
  src = fetchFromGitHub {
    owner = "shmish111";
    repo = "hriemann";
    rev = "0ccc9f8fe4d84e1280113e932c0c354fdf46ef99";
    sha256 = "116r66dgld76h38jikkrzaw0wn5aj17i8g49vy7nlcn66lwm96k3";
  };
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    base binary bytestring containers criterion hostname kazura-queue
    network protocol-buffers protocol-buffers-descriptor text time
    unagi-chan
  ];
  executableHaskellDepends = [ base ];
  testHaskellDepends = [ base ];
  homepage = "https://github.com/shmish111/hriemann";
  description = "Initial project template from stack";
  license = stdenv.lib.licenses.mit;
}
