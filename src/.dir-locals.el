
((haskell-mode . (
                  (haskell-process-path-ghci . ("nix-shell" "--run" "cabal repl lib:rcl --ghc-option=-ferror-spans --ghc-option=-fdefer-type-errors --ghc-option=-fobject-code"))
                  (dante-target . "lib:rcl")
                   )
   ))
